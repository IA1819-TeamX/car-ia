#pragma once


/**
 * @file        maquina_t.h
 *
 * @author      Alberto Delgado Soler
 * @author      Antonio Chávez López
 * @author      Tomás González Martín
 *
 * @date        October 30, 2018
 * @copyright   GNU GENERAL PUBLIC LICENSE
 *
 * @class       maquina_t maquina_t.h "include/maquina_t.h"
 * @brief       Define la maquina que representa el entorno a lo largo del tiempo
 */

#include "car_t.h"
#include "matrix_road_t.h"

class maquina_t{

    private:
        matrix_road_t matrix_;
        car_t car_;
        int nodos_generados_;
    public:
        maquina_t();
        maquina_t(string name);
        ~maquina_t();

        void randomize();
        
        int getCaminoSize();
        int getNodosGenerados();

        void camino_minimo(void);

        ostream& write(ostream& os);
        friend ostream& operator<<(ostream& os, maquina_t& m);
        maquina_t& operator=(const maquina_t& copy);


};
