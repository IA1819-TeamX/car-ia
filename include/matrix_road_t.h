#pragma once

#include "cell_t.h"
#include "car_t.h"
#include <vector>
#include <fstream>
#include <string>
#include <iostream>
#include <cstdlib>
#include <string>
#include <cassert>
/**
 * @file        matrix_road_t.h
 * 
 * @author      Alberto Delgado Soler
 * @author      Antonio Chávez López
 * @author      Tomás González Martín
 * 
 * @date        October 18, 2018
 * @copyright   GNU GENERAL PUBLIC LICENSE
 * 
 * @class       matrix_road_t matrix_road_t.h "include/matrix_road_t.h"
 * @brief       Matrix que implementa la carretera donde viaja el coche
 *   
 */

using namespace std;


#ifndef MATRIX_ROAD_T_H
#define MATRIX_ROAD_T_H


typedef vector < vector < cell_t > > matrix_t;

class matrix_road_t{
public:
    matrix_road_t(int m, int n, int percent = 5);
    matrix_road_t(ifstream& fich);
    matrix_road_t(matrix_road_t& Matrix);
    matrix_road_t();
    ~matrix_road_t();
    
    void randomize();

    matrix_t& getMatrix(void);
    pair<int,int> getFin(void);
    void showWay(vector<pair<int,int> > camino);
    
    matrix_road_t& operator=(const matrix_road_t& Matrix);
    void read_matrix(ifstream& ifs);
    
    
private:
    
    void buildRandomMatrix(int m, int n, int percent);
    
    matrix_t matrix_road_;
    int m_;
    int n_;
    pair<int,int> fin_;

    
};




#endif  //MATRIX_ROAD_T_H
