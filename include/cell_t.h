#pragma once

#include<iostream>
#include<utility>
#include<vector>
#include<cassert>

using namespace std;




/**
 * @file        cell_t.h
 * 
 * @author      Alberto Delgado Soler
 * @author      Antonio Chávez López
 * @author      Tomás González Martín
 * 
 * @date        October 18, 2018
 * @copyright   GNU GENERAL PUBLIC LICENSE
 * 
 * @class       cell_t cell_t.h "include/cell_t.h"
 * @brief       Implementación de las celdas que forman la matriz
 */
#ifndef CELL_T_H
#define CELL_T_H

class cell_t{
public:

    cell_t(int status);
    ~cell_t();

    int getStatus(void);
    void setStatus(int status);
    friend ostream& operator<<(ostream& os,cell_t c);
    void setOutStatus(cell_t pred, cell_t post);
    cell_t& operator=(const cell_t&);
    
private:
    int status_;
    string out_status_;
    /**
     * 0 -> Obstáculo
     * 1 -> Espacio libre
     * 2 -> Personas
     * 3 -> Objetivo
     */
    pair<int, int> pos_;
    
};


#endif   //CELL_T_H
