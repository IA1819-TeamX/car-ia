#pragma once

/**
 * @file        car_t.h
 * 
 * @author      Alberto Delgado Soler
 * @author      Antonio Chávez López
 * @author      Tomás González Martín
 * 
 * @date        October 18, 2018
 * @copyright   GNU GENERAL PUBLIC LICENSE
 * 
 * @class       car_t car_t.h "include/car_t.h"
 * @brief       Define el coche del proyecto
 */

using namespace std;

#include<utility>
#include<vector>
#include<iostream>
#include <cmath>


#ifndef CAR_T_H
#define CAR_T_H


class car_t{
public:
    car_t(pair<int, int> pos);
    car_t(int i, int j);
    car_t(car_t& car);
    car_t();
    ~car_t();
    
    pair<int,int> getPos(void);
    void setPos(int i, int j);
    void setPos(pair <int, int> pos);
    pair<int,int> getWayNow(int x,int y);
    vector<pair<int,int> > getWay();
    
    car_t& operator=(car_t& car);
    
    ostream& write(ostream& os);
    friend ostream& operator<<(ostream& os, car_t& Car);

    void inicioCamino(pair<int,int> fin);
    pair<int,int> findWay(int selector, int arr, int abj, int izq, int dch, pair<int,int> fin);
    void saveWay();
    
    float heurist(pair<int, int> pos, pair<int,int> fin, int selec_funct);
    
    int heu_1(pair<int, int> pos, pair<int,int> fin);
    float heu_2(pair<int, int> pos, pair<int,int> fin);
    car_t& operator=(const car_t& copy);

private:

    vector<pair<int,int> > expandidos_;
    pair<int, vector<pair<int,int> > >way_now_;
    pair<int, int> pos_;
    vector<pair<int,int> > way_;
    vector<pair<int,vector<pair<int,int> > > > caminos_;

};


#endif  //CAR_T_H
