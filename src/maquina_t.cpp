#include "maquina_t.h"
#include <cassert>

maquina_t::maquina_t():matrix_(),car_(),nodos_generados_(0){

}

void maquina_t::randomize()
{
  nodos_generados_=0;
	matrix_.randomize();
  if(matrix_.getMatrix()[car_.getPos().first][car_.getPos().second].getStatus()==0){
    matrix_.getMatrix()[car_.getPos().first][car_.getPos().second].setStatus(1);
  }
}

maquina_t::maquina_t(string name)
{
  nodos_generados_=0;
  ifstream ifs(name);
  string s;
  getline(ifs,s,',');
  int car_i = stoi(s);
  getline(ifs,s);
  int car_j = stoi(s);
  car_t coche(car_i,car_j);
  car_=coche;
  matrix_road_t matriz(ifs);
  matrix_=matriz;
  assert(matrix_.getMatrix().size()-2>car_i && 0<car_i);
  assert(matrix_.getMatrix()[0].size()-2>car_j && 0<car_j);
  if(matrix_.getMatrix()[car_i][car_j].getStatus()==0){
    matrix_.getMatrix()[car_i][car_j].setStatus(1);
  }
  
}

maquina_t::~maquina_t()
{}

int maquina_t::getCaminoSize(){
  return car_.getWay().size();
}

int maquina_t::getNodosGenerados(){
  return nodos_generados_;
}

//Realiza todo el algoritmo de la hormiga

void maquina_t::camino_minimo(void)
{
	bool terminado=false, the_final=false;
	pair<int,int> car_pos=car_.getPos();
	car_.inicioCamino(matrix_.getFin());
	while(!terminado){
	  nodos_generados_++;
		car_pos=car_.findWay(1, matrix_.getMatrix()[car_pos.first-1][car_pos.second].getStatus(),matrix_.getMatrix()[car_pos.first+1][car_pos.second].getStatus(),matrix_.getMatrix()[car_pos.first][car_pos.second-1].getStatus(),matrix_.getMatrix()[car_pos.first][car_pos.second+1].getStatus(),matrix_.getFin());

		if(car_pos.first==-1){//final de la busqueda, sin camino posible
			cout<<"No hay camino posible"<<endl;
			terminado=true;
		}
		else if(car_pos==matrix_.getFin()){//final de la busqueda, con camino posible
			cout<<"encontrado camino"<<endl;
			terminado=true;
			the_final=true;
			car_.saveWay();
			nodos_generados_+=getCaminoSize();
		}
	}

	if(car_pos==matrix_.getFin()){
		matrix_.showWay(car_.getWay());
	}
}


ostream& maquina_t::write(ostream& os)
{
  int car_i= car_.getPos().first;
  int car_j= car_.getPos().second;
  int i=0;
  while(i<matrix_.getMatrix().size()){
      int j=0;
      while(j<matrix_.getMatrix()[i].size()){
        if(i==car_i && j==car_j){
          os<<car_;
        }
        else{
          cell_t tmp=matrix_.getMatrix()[i][j];
          os<<tmp;
        }
        j++;
      }
      os<<endl;
      i++;
    }
  return os;
}

ostream& operator<<(ostream& os, maquina_t& m){
	return m.write(os);
}


maquina_t& maquina_t::operator=(const maquina_t& copy){
	this->matrix_=copy.matrix_;
	this->car_=copy.car_;
	return *this;
}

