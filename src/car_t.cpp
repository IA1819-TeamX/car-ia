#include "car_t.h"


/******************************Métodos básicos******************************/

car_t::car_t(pair<int,int> pos):
pos_(pos){}


car_t::car_t(int i, int j):
	pos_(i,j),
	way_(),
	caminos_(){
    
   // pos_.first = i;
    //pos_.second = j;
    
}

car_t::car_t(car_t& car){
    
    pos_ = car.pos_;
    way_ = car.way_;
    caminos_=car.caminos_;
    
}


car_t::car_t(void){
    
    pos_.first = 1;
    pos_.second = 1;
    
}


car_t::~car_t(void){
    
    pos_.first = -1;
    pos_.second = -1;
    
}


/***********************************Otros***********************************/

pair<int, int> car_t::getPos(void){
    
    return pos_;
}

void car_t::setPos(int i, int j){
    pos_.first = i;
    pos_.second = j;
    
}

void car_t::setPos(pair<int,int> pos){
    pos_ = pos;
}


car_t& car_t::operator =(car_t& car){
    pos_ = car.pos_;
    way_ = car.way_;
    
    return *this;
}

pair<int,int> car_t::getWayNow(int x,int y){
	pair<int,int> new_element((way_now_.second[way_now_.second.size()-1].first)-y,(way_now_.second[way_now_.second.size()-1].second)+x);
	return new_element;
}

vector<pair<int,int> > car_t::getWay(){
	return way_;
}

ostream& car_t::write(ostream& os){
    
    os << "π";
    return os;
}


ostream& operator<<(ostream& os, car_t& Car){
    Car.write(os);
    return os;
}

void car_t::inicioCamino(pair<int,int> fin){
	int cost=heurist(pos_,fin,1);
	vector<pair<int,int> > vec;
	vec.push_back(pos_);
	pair<int,vector<pair<int,int> > > primero(cost,vec);
	way_now_=primero;
	caminos_.push_back(primero);
	//cout<< "Primera posición: "<<caminos_[0].second[0].first<<", "<<caminos_[0].second[0].second<<endl;
	//cout<<"Primer coste: "<<caminos_[0].first<<endl;
}



pair<int,int> car_t::findWay(int selector, int arr, int abj, int izq, int dch, pair<int,int> fin){
	//cout<<"Creando variables"<<endl;
	
        //int selector=2;
	bool alrdy_arr=false,alrdy_abj=false,alrdy_izq=false,alrdy_dch=false;
	pair<int,int> siguiente_paso;
	vector<pair<int,vector<pair<int,int> > > >::iterator it=caminos_.begin();//aux=caminos_.begin();
	int i=0;
	//se busca el nodo que se va a inspeccionar
	for(int i=0;i<caminos_.size();i++){
		if(way_now_==(*it)){
			//cout<<"Cambio"<<endl;
			caminos_.erase(it);
			it=caminos_.begin();
			i=0;
		}
		//cout<<"luego"<<endl;
		it++;
	}
        
        bool expandido = false;
        for (int k = 0; k < expandidos_.size(); k++){
            if (getWayNow(0,0) == expandidos_[k])
                expandido = true;
        }
        if (!expandido){

            expandidos_.push_back(getWayNow(0,0));

            i=0;
            for(i=0;i<way_now_.second.size();i++){
                    if(getWayNow(0,1)==way_now_.second[i] || getWayNow(0,1)==expandidos_[i])
                            alrdy_arr=true;
                    if(getWayNow(0,-1)==way_now_.second[i] || getWayNow(0,-1)==expandidos_[i])
                            alrdy_abj=true;
                    if(getWayNow(-1,0)==way_now_.second[i] || getWayNow(-1,0)==expandidos_[i])
                            alrdy_izq=true;
                    if(getWayNow(1,0)==way_now_.second[i] || getWayNow(1,0)==expandidos_[i])
                            alrdy_dch=true;
            }

            while(i<expandidos_.size()){
                    if(getWayNow(0,1)==expandidos_[i])
                            alrdy_arr=true;
                    if(getWayNow(0,-1)==expandidos_[i])
                            alrdy_abj=true;
                    if( getWayNow(-1,0)==expandidos_[i])
                            alrdy_izq=true;
                    if(getWayNow(1,0)==expandidos_[i])
                            alrdy_dch=true;
                    i++;
            }

            if(arr!=0 && !alrdy_arr){
                    pair<int,vector<pair<int,int> > > insertando=way_now_;
                    insertando.second.push_back(getWayNow(0,1));
                    insertando.first=heurist(getWayNow(0,1),fin,selector)+insertando.second.size()-1;
                    caminos_.insert(caminos_.begin(),insertando);
            }
            if(abj!=0 && !alrdy_abj){
                    pair<int,vector<pair<int,int> > > insertando=way_now_;
                    insertando.second.push_back(getWayNow(0,-1));
                    insertando.first=heurist(getWayNow(0,-1),fin,selector)+insertando.second.size()-1;
                    caminos_.insert(caminos_.begin(),insertando);
            }
            if(izq!=0 && !alrdy_izq){
                    pair<int,vector<pair<int,int> > > insertando=way_now_;
                    insertando.second.push_back(getWayNow(-1,0));
                    insertando.first=heurist(getWayNow(-1,0),fin,selector)+insertando.second.size()-1;
                    caminos_.insert(caminos_.begin(),insertando);
            }
            if(dch!=0 && !alrdy_dch){
                    pair<int,vector<pair<int,int> > > insertando=way_now_;
                    insertando.second.push_back(getWayNow(1,0));
                    insertando.first=heurist(getWayNow(1,0),fin,selector)+insertando.second.size()-1;
                    caminos_.insert(caminos_.begin(),insertando);
            }
        
        }

	it=caminos_.begin();
	way_now_=(*it);
	//se busca el siguiente nodo que se va a inspeccionar
	for(it=caminos_.begin();it!=caminos_.end();it++){
		if(way_now_.first>it->first)
			way_now_=(*it);
	}
	siguiente_paso=way_now_.second[way_now_.second.size()-1];
	
        
        
        if(caminos_.begin()==caminos_.end()){
		siguiente_paso=make_pair (-1,-1);
                
                
        }

        
    return siguiente_paso;
}

void car_t::saveWay(){
	way_=way_now_.second;

}

float car_t::heurist(pair<int, int> pos, pair<int,int> fin, int selec_funct){
    if(selec_funct==1){
        return heu_1(pos,fin);
    }
    else if(selec_funct==2){
        return heu_2(pos,fin);
    }
}

int car_t::heu_1(pair<int, int> pos, pair<int,int> fin){
    return (abs(pos.first - fin.first) + abs(pos.second - fin.second));
}

float car_t::heu_2(pair<int, int> pos, pair<int,int> fin){
    int c1 = (pos.first - fin.first) * (pos.first - fin.first);
    int c2 = (pos.second - fin.second) * (pos.second - fin.second);
    return sqrt(c1  + c2);

}

car_t& car_t::operator=(const car_t& copy){
	this->caminos_=copy.caminos_;
	this->expandidos_= copy.expandidos_;
	this->way_= copy.way_;
	this->pos_= copy.pos_;
	this->way_now_= copy.way_now_;
	return *this;
}
