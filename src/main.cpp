#include <iostream>
#include <stdlib.h>
#include <ctime>
#include "maquina_t.h"



using namespace std;

//void menu (void);

int main (int argc, char *argv[]){

    srand(time(NULL));
    
    
    cout << "Práctica: Estrategias de Búsqueda \t Inteligencia artificial 2018-19" << endl;
    cout << "Autores:" << endl;
    cout << "Antonio Chávez López" << endl;
    cout << "Alberto Delgado Soler" << endl;
    cout << "Tomás González Martín" << endl;
    cout << "Pulse \"enter\" para continuar" << endl;
    cin.get();
    system("clear");
    
    
    
    maquina_t Maquina;
            
    
    
    if (argc == 2){ //Lectura de ficheros
        
        string file = argv[1];
        maquina_t Maquina_copy(file);
        Maquina = Maquina_copy;
    
    }
    else{
    	Maquina.randomize();
    }
    remove("matrix.txt");
    ofstream fs("matrix.txt");
    
    
    
    
    cout << Maquina;
    cout<<"Buscando camino mínimo"<<endl;
    clock_t timeCPU=clock();
    Maquina.camino_minimo();
    timeCPU=clock()-timeCPU;
    cout << Maquina;
    cout << "Tiempo de CPU consumido ="<<(float)timeCPU/CLOCKS_PER_SEC<<" segundos"<<endl;
    cout << "Tamaño del camino mínimo ="<<Maquina.getCaminoSize()<<endl;
    cout << "Número de nodos generados ="<<Maquina.getNodosGenerados()<<endl;
    cout << "Autores:" << endl;
    cout << "Antonio Chávez López" << endl;
    cout << "Alberto Delgado Soler" << endl;
    cout << "Tomás González Martín" << endl;
    
    return 0;

}
