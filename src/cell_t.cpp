#include "cell_t.h"


/******************************Métodos básicos******************************/

cell_t::cell_t(int status):
status_(status){
	setStatus(status);
}

cell_t::~cell_t(void){
    status_ = -1;
}

/***********************************Otros***********************************/

int cell_t::getStatus(void){
    
    return status_;
}

cell_t& cell_t::operator=(const cell_t& copia){
	this->setStatus(copia.status_);
	return *this;
}

void cell_t::setStatus(int status){

	status_ = status;
	if(getStatus()==0)
		out_status_ = "░";
	if(getStatus()==1 || getStatus()== 2)
		out_status_ = " ";
	if (getStatus()== 3)
		out_status_ = "X";
    
}

void cell_t::setOutStatus(cell_t pred, cell_t post){
	assert(getStatus()==1 || getStatus()== 2);

            out_status_ = "█";
}


ostream& operator <<(ostream& os,cell_t c){

	os<<c.out_status_;


	return os;
}


