#include "matrix_road_t.h"

/******************************Métodos básicos******************************/
matrix_road_t::matrix_road_t(int m, int n, int percent):
m_(m+2),
n_(n+2)
{

        buildRandomMatrix(m_, n_, percent);
        int a=rand() % (m-2)+1;
        int b=rand() % (n-2)+1;
        pair<int, int> final(a,b);
        fin_=final;
        matrix_road_[fin_.first][fin_.second].setStatus(3);

}




matrix_road_t::matrix_road_t(ifstream& fich){
       
    read_matrix(fich);
    
}


matrix_road_t::matrix_road_t(matrix_road_t& Matrix):
matrix_road_(Matrix.matrix_road_),
m_(Matrix.m_),
n_(Matrix.n_),
fin_(Matrix.fin_)
{}
matrix_road_t::matrix_road_t():matrix_road_(),m_(0),n_(0),fin_(){

}

void matrix_road_t::randomize(){
    

    
    m_ = rand () % 80 + 20;
    n_ = rand () % 80 + 20;

    buildRandomMatrix(m_, n_, 20);
    
    
    //crear la meta de manera aleatoria
    int a=rand() % (m_-2)+1;
	int b=rand() % (n_-2)+1;
	pair<int, int> final(a,b);
	fin_=final;
	matrix_road_[fin_.first][fin_.second].setStatus(3);

    
}


matrix_road_t::~matrix_road_t(void){
    matrix_road_.clear();
}




/***********************************Otros***********************************/

matrix_t& matrix_road_t::getMatrix(void){
    
    return matrix_road_;
}

pair<int,int> matrix_road_t::getFin(void){
	return fin_;
}

void matrix_road_t::showWay(vector<pair<int,int> > camino){
	for(int i=1;i<camino.size()-1;i++){
		matrix_road_[camino[i].first][camino[i].second].setOutStatus(matrix_road_[camino[i-1].first][camino[i-1].second],matrix_road_[camino[i+1].first][camino[i+1].second]);
	}
}


matrix_road_t& matrix_road_t::operator =(const matrix_road_t& Matrix){
    this->matrix_road_ = Matrix.matrix_road_;
    this->m_ = Matrix.m_;
    this->n_ = Matrix.n_;
    this->fin_ = Matrix.fin_;
    return *this;
}



void matrix_road_t::read_matrix(ifstream& ifs){// sin terminar
	string s;
    int m, n;       //filas y colunmas
	int i, j;       //coordenadas a leer
    int finish_line_i, finish_line_j;   //coordenadas leídas de la meta
        

        
	if(ifs.is_open()){
            //Dimensiones de la matrix
            ifs >> m;
            m_ = m + 2;
            ifs >> n;
            n_ = n + 2;
            
            //Leer coche
            /*getline(ifs,s,',');
            car_i = stoi(s);
            getline(ifs,s);
            car_j = stoi(s);*/

            //Leer meta
            getline(ifs,s,',');
            finish_line_i = stoi(s);
            getline(ifs,s);
            finish_line_j = stoi(s);
            
            
            //Construir la matriz sin obstáculos
            buildRandomMatrix(m_, n_, 0);
            
            pair<int, int> final(finish_line_i,finish_line_j);
            fin_=final;
            matrix_road_[fin_.first][fin_.second]=3;


            
                while(!ifs.eof()){
                    getline(ifs,s,',');
                    if(!ifs.eof()){
                        i = stoi(s);
                        getline(ifs,s);
                        j = stoi(s);


                        //Si el obstáculo coincide con la meta
                        assert (finish_line_i != i || finish_line_j != j);
                        //cout << matrix_road_[i][j] << endl;
                                
                        //Se crea el obstáculo
                        matrix_road_[i][j]=0;
                    }
                }
	}

	
}

/********************************Métodos privados********************************/

void matrix_road_t::buildRandomMatrix(int m, int n, int percent){
    
    
    
    //Inicializada con osbtáculos
    for (int i = 0; i < m; i++){
        vector <cell_t> vector_cell;
        
        matrix_road_.push_back(vector_cell);
        for (int j = 0; j < n; j++){
            cell_t Cell(0);
            matrix_road_[i].push_back(Cell);
        }
       

    }
    
   
    int i, j;
    int it = 0;
    
    
    //Se asignan espacios libres
    while (it < ((m-2)*(n-2)*(100 - percent)/100)){
        i = rand() % (m-2)+1;
        j = rand() % (n-2)+1;
        
        if (matrix_road_[i][j].getStatus()!= 2 && matrix_road_[i][j].getStatus()!= 1){
            matrix_road_[i][j].setStatus(rand() % 2+1);
            it++;
        }
        
    }
    
    
}

