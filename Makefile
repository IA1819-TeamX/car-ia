
#############################################################
################ Makefile for car-ia project ################
#############################################################

TARGET = car-ia

SRC = src
INC = include
BIN = bin

SOURCE = $(wildcard $(SRC)/*.cpp)
OBJECT = $(patsubst %,$(BIN)/%, $(notdir $(SOURCE:.cpp=.o)))


## Colours
RED = \033[0;31m
BRED = \033[1;31m
BPURPLE = \033[1;35m
BBLUE = \033[1;34m
BGREEN = \033[1;32m
CYAN = \033[0;36m


NC = \033[0;0m

CC = g++
CFLAGS = -Wall -std=c++11 -I$(INC)
$(BIN)/$(TARGET)	:	$(OBJECT)
	@echo "$(BBLUE)Linking...$(NC)"
	$(CC) -o $@ $^
	@echo "$(BGREEN)Finished!$(NC)"
	@echo " "


$(BIN)/%.o : $(SRC)/%.cpp
	@echo "$(CYAN)Compiling...$(NC)"
	$(CC) $(CFLAGS) -c $< -o $@
	@echo "$(BGREEN)Finished!$(NC)"
	@echo " "

.PHONY : help run clean

run : $(BIN)/$(TARGET)
	@echo "$(BPURPLE)Running...$(NC)"
	$(BIN)/$(TARGET) $(FICH)
	@echo "$(BGREEN)Done!!$(NC)"
	@echo " "

clean :
	@echo "$(RED)Cleaning...$(NC)"
	rm -f $(OBJECT) $(BIN)/$(TARGET)
	@echo "$(BRED)Done!!$(NC)"
	@echo " "
# tap make help to see the help
help : 
	@echo "src: $(SOURCE)"
	@echo "obj: $(OBJECT)"
