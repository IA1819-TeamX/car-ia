# car-ia 
## Find the path for the car
**Authors**
* Alberto Delgado Soler
* Antonio Chávez López
* Tomás González Martín

## Short project description
Implementation of the path a car follows by taking the best possible path considering heuristic algorithms
